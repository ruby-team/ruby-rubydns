% RUBYDNS-CHECK(1) Rubydns-check Man Page
% Philippe Thierry
% June 2017
# NAME

rubydns-check - test and check DNS servers.

# SYNOPSIS
**rubydns-check [options]**


# DESCRIPTION

**rubydns-check** is designed to test and check DNS servers.

# OPTIONS:

**-s, --server ns.my.domain.**
The DNS server to query.

**-d, --domain my.domain.**
The DNS zone to transfer/test.

**-f, --fetch output.yml**
Pull down a list of hosts. Filters TXT  and HINFO records. DNS transfers must be enabled.

**-c, --check input.yml**
Check that the DNS server returns results as specified by the file.

**-q, --query input.yml**
Query the remote DNS server with all hostnames in the given file, and checks the IP addresses are consistent.

**-p, --ping input.yml**
Ping all hosts to check if they are available or not.

**-r, --reverse input.yml**
Check that all address records have appropriate reverse entries.

**--copy**
Display copyright information

**-h, --help**
Show this help message.


# HISTORY
June 2017, Man page originally compiled by Philippe Thierry (phil at reseau-libre dot com)
